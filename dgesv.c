#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "mkl_lapacke.h"
#include "wtime.c"

//OK
double absolute(double number ){
  double res;
    if(number<0){
        res = (-1)*number;
     }else{
      res = number;
      }
      return res;
}


void generate_matrix(int size,double *matrix)
{
    int i;
    srand(1);

    for (i = 0; i < size * size; i++)
    {
        matrix[i] = rand() % 100;
    }
}

void print_matrix(const char *name, double *matrix, int size)
{
    int i, j;
    printf("matrix: %s \n", matrix);

    for (i = 0; i < size; i++)
    {
            for (j = 0; j < size; j++)
            {
                printf("%f ", matrix[i * size + j]);
            }
            printf("\n");
    }
}

int check_result(double *bref, double *b, int size) {
    int i;
   double  epsi;
    epsi = 0.005;

    for(i=0;i<size*size;i++) {
        if (absolute(bref[i]-b[i]) > epsi) return 0;
    }
    return 1;
}

//New code 

//OK
double *selectLigne(double* a, int indiceLigne, int size){
  double* ligneSelect = malloc(size*sizeof(double*));
  int j;
  
  for(j=0;j<size;j++){
    ligneSelect[j] = a[indiceLigne*size + j];
  }
  return ligneSelect;
}


//OK
void EchangeLigne(double *a, double *MatID, int size, int indiceLigne_k , int indiceLigne_r){
  
  double *tmp = malloc(size*sizeof(double*));
  double *tmp2 = malloc(size*sizeof(double*));

  double *tmp_ID = malloc(size*sizeof(double*));
  double *tmp2_ID = malloc(size*sizeof(double*));
  
  int j;
  tmp = selectLigne(a,indiceLigne_k,size);
  tmp2 = selectLigne(a,indiceLigne_r,size);

  tmp_ID = selectLigne(MatID,indiceLigne_k,size);
  tmp2_ID = selectLigne(MatID,indiceLigne_r,size);
  
  for(j=0;j<size;j++){
    a[indiceLigne_r*size + j] = tmp[j];
    a[indiceLigne_k*size + j] = tmp2[j];

    MatID[indiceLigne_r*size+j]=tmp_ID[j];
    MatID[indiceLigne_k*size+j]=tmp2_ID[j];
  }
free(tmp);
free(tmp2);
free(tmp_ID);
free(tmp2_ID);
}

//OK
void divide (double *a,double* MatID, int indiceLigne_k, int size,int indiceColumn){
  double *ligne_k = malloc(size*sizeof(double*));
  double *ligne_kID = malloc(size*sizeof(double*));
  double *LigneRes = malloc(size*sizeof(double*));
  double *LigneRes_ID = malloc(size*sizeof(double*));
  
  int j;
  double scalaire;

  ligne_k = selectLigne(a,indiceLigne_k,size);
  ligne_kID = selectLigne(MatID,indiceLigne_k,size);

  scalaire = a[indiceLigne_k*size+indiceColumn];
  for(j=0;j<size;j++){
    if(scalaire!=0){
      LigneRes[j] = ligne_k[j]/scalaire;
      LigneRes_ID[j]= ligne_kID[j]/scalaire;
  }
  }

  for(j=0;j<size;j++){
    a[indiceLigne_k*size + j] = LigneRes[j];
    MatID[indiceLigne_k*size + j] = LigneRes_ID[j];
    
  }
free(ligne_k);
free(ligne_kID);
free(LigneRes);
free(LigneRes_ID);

}

void Soustraire(double *a, double *MatID, int indiceLigne_i, int indiceLigne_r, int size,int indiceColumn){
    int j;
  double scalaire;

  scalaire = a[indiceLigne_i*size + indiceColumn];

  for(j=0;j<size;j++){
    a[indiceLigne_i*size + j] = a[indiceLigne_i*size + j] -(scalaire*a[indiceLigne_r*size+j]);
    MatID[indiceLigne_i*size + j] = MatID[indiceLigne_i*size + j] -(scalaire*MatID[indiceLigne_r*size+j]);
  }
}



//OK
double findMax(double*a,int size,int indiceColumn,int r){
  double maximum;
  int i;
  double tmp;

  maximum = a[0*size + indiceColumn];
  maximum = absolute(maximum);
  
  for(i=r+1;i<size;i++){
    tmp = a[i*size+ indiceColumn];
    tmp = absolute(tmp);

    if(tmp<maximum){
      maximum = maximum;
    }else{
      maximum = tmp;
    }
  }
  return maximum;
}


//OK
int maximunIndex(double*a,int size,int indiceColumn,double maximum,int r){
  int index;
  int i;
  double tmp;
  index =0;
  
  for(i=r+1;i<size;i++){
    tmp = a[i*size+ indiceColumn];
    tmp = absolute(tmp);
    if(tmp>=maximum){
      index =i;
    }
  }
  return(index);
}

//OK
double *generateMatID(int size){
  int i,j;
  double *MatID=(double *)malloc(sizeof(double) * size * size);

  for(i=0;i<size;i++){
    for(j=0;j<size;j++){
      if(i==j){
	MatID[i*size + j]=1;
      }else{
	MatID[i*size + j]=0;
      }
    }
  }
  return MatID;
}

/////////////////////////////////////////////////////////////////////////////////////////////

void gaussMethod(double * a, double* MatID, int size){
  int r;
  int i;
  int j;
  int k;
  double maxi;  
  r=-1;
#pragma omp parallel private(maxi,k,i), shared(size,MatID,a,r)
{
#pragma omp for
  for(j=0;j<size;j++){
    maxi = findMax(a,size,j,r);
    k=maximunIndex(a,size,j,maxi,r);
    if(a[k*size+j]!=0){
      r=r+1;
      divide(a,MatID,k,size,j);
      EchangeLigne(a,MatID,size,k ,r);
      for(i=0;i<size;i++){
	if(i!=r){
	  Soustraire(a,MatID,i,r,size,j);
	}
      }
    }
   }  
}
}


//////////////////////////////////////////////////////////////////////////////////////////
double *multiplicationOF2Mat(double *a, double *b,int size){
  double *Matres = (double *)malloc(sizeof(double) * size * size);
  int i;
  int j;
  int k;
  for(i=0;i<size;i++){
    for(j=0;j<size;j++){
      Matres[i*size+j]= 0;
      for(k=0;k<size;k++){
	Matres[i*size+j]= Matres[i*size+j] +(a[i*size+k]*b[k*size+j]);	
      }
    }
  }
  return(Matres);
}

/////////////////////////////////////////////////////////////////////////////////////////
//
double *computeEquation(double *a, double*b, int size){
  double *MatID;
  double *Matres  = (double *)malloc(sizeof(double) * size * size);
  MatID = generateMatID(size);
double time = wtime();
  gaussMethod(a,MatID,size);
time=wtime()-time;
  Matres = multiplicationOF2Mat(MatID,b,size);
printf("Time taken by the parallelize %lf secondes \n",time);

return(Matres);
free(Matres);
}


////////////////////////////////////////////////////////////////////////////////////////////

double *my_dgesv(double *a, double *b, int size) {
    //Replace with your implementation
    double *Matres  = (double *)malloc(sizeof(double) * size * size);
   Matres =  computeEquation(a,b,size);
   return(Matres);
free(Matres);	
}

//////////////////////////////////////////////////////////////////////////////////////////////

    void main(int argc, char *argv[])
    {

        int size = atoi(argv[1]);
	
	double *Matres = (double *)malloc(sizeof(double) * size * size);
double *a = (double *)malloc(sizeof(double) * size * size);
double *b = (double *)malloc(sizeof(double) * size * size);
double *aref = (double *)malloc(sizeof(double) * size * size);
double *bref = (double *)malloc(sizeof(double) * size * size);
	
        generate_matrix(size,a);
        generate_matrix(size,aref);        
        generate_matrix(size,b);
        generate_matrix(size,bref);
        

        //print_matrix("A", a, size);
        //print_matrix("B", b, size);

        // Using MKL to solve the system
        MKL_INT n = size, nrhs = size, lda = size, ldb = size, info;
        MKL_INT *ipiv = (MKL_INT *)malloc(sizeof(MKL_INT)*size);

        clock_t tStart = clock();
        info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, aref, lda, ipiv, bref, ldb);
	
	////////////////////////////////

        printf("Time taken by MKL: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

        tStart = clock();    
        MKL_INT *ipiv2 = (MKL_INT *)malloc(sizeof(MKL_INT)*size); 
       

	Matres = my_dgesv(a,b,size);
        printf("Time taken by my implementation: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
        
        if (check_result(bref,Matres,size)==1)
            printf("Result is ok!\n");
        else    
            printf("Result is wrong!\n");
        
      //print_matrix("Matres", Matres, size);
       //print_matrix("Xref", bref, size);
       free(Matres);
	free(a);
	free(b);
	free(aref);
	free(bref);
    }




